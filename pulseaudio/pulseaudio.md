# Building Pulseuaudio from source on a Pi

Install deps

See https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/Developer/PulseAudioFromGit/

```bash
sudo apt update
sudo apt install dh-exec doxygen dpkg-dev intltool libapparmor-dev  libasound2-dev  libasyncns-dev libavahi-client-dev libbluetooth-dev libsbc-dev  libcap-dev  libfftw3-dev libglib2.0-dev libgtk-3-dev libice-dev libjack-dev liblirc-dev libltdl-dev liborc-0.4-dev  libsamplerate0-dev libsndfile1-dev  liblircclient-dev libltdl-dev liborc-0.4-dev libsnapd-glib-dev  libsndfile1-dev libsoxr-dev libspeexdsp-dev  libssl-dev libsystemd-dev  libtdb-dev libudev-dev  libwebrtc-audio-processing-dev   libwrap0-dev libx11-xcb-dev libxcb1-dev libxml2-utils libxtst-dev systemd libgtkmm-3.0-1v5 libgtkmm-3.0-dev git gstreamer1.0-gtk3  gstreamer1.0-pulseaudio gstreamer1.0-plugins-base gstreamer1.0-x libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio libcanberra-gtk3-dev lynx
```

Clone repos for `pulseaudio` and `paprefs`

```bash

git clone https://gitlab.freedesktop.org/pulseaudio/paprefs.git
git clone https://gitlab.freedesktop.org/pulseaudio/pulseaudio.git
```

Build `pulseadio`

```bash
cd pulseadio
meson build
sudo ninja -C build install
# Copy schema and recompile for glib
sudo cp ./src/modules/gsettings/org.freedesktop.pulseaudio.gschema.xml /usr/share/glib-2.0/schemas/
sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
```

Build `paprefs`

```bash
cd ~/paprefs
meson build
sudo ninja -C build install
```

Reboot